package main

import (
	"encoding/json"
	"fmt"
	"net/http"
	"reflect"
	"strings"

	"github.com/spf13/viper"
)

type Conf struct {
	IconURL  string                       `json:"icon_url"`
	Username string                       `json:"username"`
	Port     string                       `json:"port"`
	Funcs    map[string]map[string]string `json:"funcs"`
	RPCBot   map[string]string            `json:"rpcbot"`
}

type Response struct {
	ResponseType string `json:"response_type"`
	IconURL      string `json:"icon_url"`
	Username     string `json:"username"`
	Text         string `json:"text"`
}

type JResponse struct {
	Response []byte
}

var (
	Cf Conf
)

func (j *JResponse) Reply(w http.ResponseWriter, rtype string, text string) {
	var err error

	j.Response, err = json.Marshal(Response{
		ResponseType: rtype,
		IconURL:      Cf.IconURL,
		Username:     Cf.Username,
		Text:         text,
	})

	if err != nil {
		fmt.Fprintf(w, "error building response: %s\n", err)
		return
	}
	w.Header().Add("Content-Type", "application/json")

	fmt.Fprintf(w, string(j.Response))
}

// Publicly reply as comment
func (j *JResponse) pubReply(w http.ResponseWriter, msg string) {
	j.Reply(w, "comment", msg)
}

// Privately reply as ephemeral
func (j *JResponse) privReply(w http.ResponseWriter, msg string) {
	j.Reply(w, "ephemeral", msg)
}

func main() {
	viper.SetConfigName(".derpina")
	viper.SetConfigType("yaml")
	viper.AddConfigPath("$HOME")
	viper.AddConfigPath(".")

	err := viper.ReadInConfig()
	if err != nil {
		panic(fmt.Errorf("Fatal error config file: %s\n", err))
	}

	err = viper.Unmarshal(&Cf)
	if err != nil {
		panic(fmt.Errorf("Unable to load config file: %s\n", err))
	}

	botname := viper.GetString("username")

	http.HandleFunc("/"+botname, func(w http.ResponseWriter, r *http.Request) {
		var j JResponse

		err := r.ParseForm()
		if err != nil {
			j.pubReply(w, err.Error())
			return
		}
		fmt.Printf("%+v\n", r.Form)
		if len(r.Form["user_name"]) < 1 {
			j.pubReply(w, "empty username")
			return
		}
		text := r.Form["text"][0]
		user := r.Form["user_name"][0]

		// add bot name for slash commands
		if !strings.Contains(text, botname) {
			text = "/" + botname + " " + text
		}

		m := strings.Split(text, " ")
		// m[0] is bot's name, m[1] is the function
		if len(m) < 2 {
			j.pubReply(w, "no arguments given")
		}
		// build functio name
		fname := Cf.Funcs[m[1]]["name"]
		// build function arguments
		va := []reflect.Value{
			reflect.ValueOf(w),
			reflect.ValueOf(user),
			reflect.ValueOf(text),
		}
		f := reflect.ValueOf(&j).MethodByName(fname)
		if f.IsValid() {
			f.Call(va)
		} else {
			j.pubReply(w, fmt.Sprintf("%s: %s\n", fname, f.String()))
		}
	})

	err = http.ListenAndServe(Cf.Port, nil)
	if err != nil {
		panic(err)
	}
}
