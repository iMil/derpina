package main

import (
	"net/http"
)

func (j *JResponse) Help(w http.ResponseWriter, user string, text string) {
	text = "Available functions:"
	for k, v := range Cf.Funcs {
		text += "\n`" + k + "`: " + v["help"]
	}
	j.privReply(w, text)
}
