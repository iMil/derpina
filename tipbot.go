package main

import (
	"encoding/base64"
	"fmt"
	"log"
	"net/http"
	"strconv"
	"strings"
	"time"

	"github.com/ybbus/jsonrpc"
)

type Tx struct {
	Address string  `json:"address"`
	Amount  float64 `json:"amount"`
	Ttime   int64   `json:"time"`
	Txid    string  `json:"txid"`
}

// Set target user
func getUser(user string, words []string, nwords int) string {
	if nwords > 3 {
		return words[3]
	}
	return user
}

// Get balance for user
func getBalance(rpcClient jsonrpc.RPCClient, user string) (float64, error) {
	r, err := rpcClient.Call("getbalance", user)
	if err != nil {
		return 0, err
	}
	balance, err := r.GetFloat()
	if err != nil {
		return 0, err
	}
	return balance, nil
}

// Get all addresses for user
func getAddresses(rpcClient jsonrpc.RPCClient, user string) ([]string, error) {
	addrs := []string{}
	r, err := rpcClient.Call("getaddressesbyaccount", user)
	if err != nil {
		return addrs, err
	}
	err = r.GetObject(&addrs)
	if err != nil {
		return addrs, err
	}
	if len(addrs) < 1 {
		return addrs, fmt.Errorf("no address associated with account")
	}
	return addrs, nil
}

// Get base / original / first address
func getBaseAddress(rpcClient jsonrpc.RPCClient, user string) (string, error) {
	addrs, err := getAddresses(rpcClient, user)
	if err != nil {
		return "", err
	}
	return addrs[len(addrs)-1], nil
}

// List transactions for user
func listTransactions(rpcClient jsonrpc.RPCClient, user string) ([]Tx, error) {
	var txs []Tx
	r, err := rpcClient.Call("listtransactions", user)
	if err != nil {
		return txs, err
	}
	err = r.GetObject(&txs)
	if err != nil {
		return txs, err
	}
	return txs, nil
}

// Send amount from user to dest
func sendFrom(rpcClient jsonrpc.RPCClient, user, dest string, amount int) (string, error) {
	if amount < 0 {
		return "", fmt.Errorf("can't tip negative amnunts")
	}
	balance, err := getBalance(rpcClient, user)
	if err != nil {
		return "", err
	}
	if amount > int(balance) {
		return "", fmt.Errorf("not enough funds")
	}
	r, err := rpcClient.Call("sendfrom", user, dest, amount)
	if err != nil {
		return "", err
	}
	txid, err := r.GetString()
	if err != nil {
		return "", err
	}
	return txid, nil
}

func (j *JResponse) Tipbot(w http.ResponseWriter, user string, text string) {
	words := strings.Fields(text)
	nwords := len(words)
	// !botname funcname command
	if nwords < 3 {
		j.pubReply(w, "missing parameter")
		return
	}
	var err error

	funcname := words[1]
	cmd := words[2]

	host := "http://" + Cf.RPCBot["host"]
	auth := Cf.RPCBot["user"] + ":" + Cf.RPCBot["password"]

	rpcClient := jsonrpc.NewClientWithOpts(host, &jsonrpc.RPCClientOpts{
		CustomHeaders: map[string]string{
			"Authorization": "Basic " + base64.StdEncoding.EncodeToString([]byte(auth)),
		},
	})

	switch cmd {
	case "getblockcount":
		// Get the current block count.
		r, err := rpcClient.Call(cmd)
		if err != nil {
			log.Fatal(err)
		}
		blockCount, err := r.GetInt()
		if err != nil {
			j.pubReply(w, err.Error())
			return
		}
		text = fmt.Sprintf("block count: %d\n", blockCount)
	case "getbalance":
		balance, err := getBalance(rpcClient, user)
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		j.privReply(w, fmt.Sprintf("balance: %f", balance))
	case "tip":
		// !botname funcname tip <user> <amount> [dryrun]
		if nwords < 5 {
			j.pubReply(w, "missing parameter")
			return
		}

		totip := strings.Replace(words[3], "@", "", 1)
		tipamount := words[4]

		dest, err := getBaseAddress(rpcClient, totip)
		if err != nil {
			j.pubReply(w, err.Error())
			return
		}
		amount, err := strconv.Atoi(tipamount)
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		txid := "w0w"
		if len(words) < 6 { // for debugging without sending
			txid, err = sendFrom(rpcClient, user, dest, amount)
			if err != nil {
				j.privReply(w, err.Error())
				return
			}
		}
		// compute generodity factor: number of badges to print
		gfact, err := strconv.Atoi(Cf.Funcs[funcname]["generosityfactor"])
		if err != nil {
			j.pubReply(w, err.Error())
			return
		}
		ncoins := int(amount / gfact)
		icons := strings.Repeat(":"+Cf.Funcs[funcname]["icon"]+":", ncoins)
		text = Cf.Funcs[funcname]["thankphrase"] + " " + icons + "\n\n"
		text += "@" + user + ": " + "you sent " + tipamount + " "
		text += Cf.Funcs[funcname]["coin"] + " to " + totip + " (" + dest + ")\n"
		text += "txid: " + Cf.Funcs[funcname]["explorer"] + "/" + txid + "\n"
		text += "thanks!"
		j.pubReply(w, text)
	case "getaddress":
		text, err = getBaseAddress(rpcClient, getUser(user, words, nwords))
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		j.privReply(w, text)
	case "listtransactions":
		txs, err := listTransactions(rpcClient, getUser(user, words, nwords))
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		text = "Last 10 transactions:\n\n"
		for i, tx := range txs {
			text += "**date**: " + time.Unix(tx.Ttime, 0).Format(time.RFC822Z) + "\n"
			text += "**destination**: " + tx.Address + "\n"
			text += fmt.Sprintf("**amount**: %f\n", tx.Amount)
			text += "**txid**: " + Cf.Funcs[funcname]["explorer"] + "/" + tx.Txid + "\n\n"
			if i > 10 {
				break
			}
		}
		j.privReply(w, text)
	case "send":
		if nwords < 5 {
			j.privReply(w, "missing parameter")
			return
		}
		amount, err := strconv.Atoi(words[4])
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		txid, err := sendFrom(rpcClient, user, words[3], amount)
		if err != nil {
			j.privReply(w, err.Error())
			return
		}
		j.privReply(w, "txid: "+txid)
	default:
		text = "unknown command"
		if words[0][0] == '!' {
			j.pubReply(w, text)
		} else {
			j.privReply(w, text)
		}
	}
}
