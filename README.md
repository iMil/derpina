# derpina

Modular Go bot for [Mattermost outgoing webhooks](https://developers.mattermost.com/integrate/outgoing-webhooks/).

## Init

* Build

Obviously _golang_ and `make` are needed

```sh
$ make
```

* Tune `~/.derpina.yaml`

Format:
* `port`: _TCP_ port where the bot will listen for queries
* `icon_url`: bot's avatar _URL_
* `username`: the bot's username in [Mattermost][1]
* `funcs`: hash of available functions
  * `<trigger name>`: the function name called by the user
    * `name`: the actual _golang_ function name
    * `help`: help about the command

For the tipping function:
* `icon`: [Mattermost][1] emoji to be used when a tip is given
* `generosityfactor`: a ratio to divide the amount tipped in order to repeat a number of `icon`s
* `coin`: currency's name
* `thankphrase`: the phrase the bot will say when a tip is given
* `explorer`: the block explorer to link the transaction to

## Launch

```
$ ./derpina
```

## Interact with the bot

```
!derpina help
```

Available functions:

* `doge`: cryptocurrency tipping machine

The `doge` function is a _JSON-RPC_ client that will query the `host` specified in the `rpcbot` section of `~/.derpina.yaml` file, using a `user` and a `password`.  
By default it uses the [DOGE currency](https://dogecoin.com/) but it could very easily be converted to any other cryptocurrency with a compatible daemon, only by modifying the configuration file.

Of course, before using this function, it is necessary to create _DOGE_ addresses and import them to the node the bot will query. In order for the tipping system to work, the node `account` names should match [Mattermost][1] usernames.

A convenient way of creating customized addresses is by using [vanitygen-plus](https://github.com/exploitagency/vanitygen-plus), which permits creating many cryptocurrencies types with a custom prefix. Example usage:
```sh
$ ./vanitygen -C DOGE DGee
Generating DOGE Address
Difficulty: 78508
DOGE Pattern: DGee
DOGE Address: DGeegSzvCRfJozjz7EqcQFvF2jtYssLNmo
DOGE Privkey: 6JBKzxF3cFGMwuHGU3TNAZnHabhQW1YwpzarnciGuLRQd8UKJ7J
```
Then import this address into the node's wallet:
```sh
$ dogecoin-cli importprivkey "6JBKzxF3cFGMwuHGU3TNAZnHabhQW1YwpzarnciGuLRQd8UKJ7J" "mmuser" false
```
Once done, the user with the username `mmuser` can tip and be tipped.

### Usage

See own balance privately
```
/derpina doge getbalance
```
See own or account address privately
```
/derpina doge getaddress [account]
```
Privately see last 10 transactions for self or account if specified
```
/derpina doge listtransactions [account]
```
Send coins to an address privately
```
/derpina doge send <address> <amount>
```
Tip someone publicly
```
!derpina doge tip <user> <amount> [dryrun]
```
If `dryrun` parameter is set, the transaction will not take place. Essentially for debugging purposes.

## Links

* https://en.bitcoin.it/wiki/API_reference_(JSON-RPC)
* https://en.bitcoin.it/wiki/Original_Bitcoin_client/API_calls_list
* https://github.com/ybbus/jsonrpc
* https://godoc.org/github.com/btcsuite/btcd/rpcclient (not used but inspirational)


[1]: https://mattermost.com/
