package main

func InSlice(s []string, m string) bool {
	for _, k := range s {
		if k == m {
			return true
		}
	}
	return false
}
