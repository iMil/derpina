module gitlab.com/iMil/derpina

go 1.13

require (
	github.com/spf13/viper v1.7.1
	github.com/ybbus/jsonrpc v2.1.2+incompatible
)
